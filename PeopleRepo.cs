﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    public class PeopleRepo
    {
        public struct Person
        {
            public int index;
            public string name;
            public DateTime birthdaydate;
            public DateTime? diedate;
            public int indexmom;
            public int indexdad;
        }
    }
}
