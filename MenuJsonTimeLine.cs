﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class MenuJsonTimeLine : TimelineEventsRepo
    {
       public static void AddAnEventJson()
        {
            string timeline_file = File.ReadAllText("timeline.json");
            TimelineEvent[] listtim = JsonConvert.DeserializeObject<TimelineEvent[]>(timeline_file);
            string[] stroklist = new string[listtim.Length];
            for (int i = 0; i < listtim.Length; i++) stroklist[i] = listtim[i].timeevent.ToString() + ";" + listtim[i].event1.ToString() + ";" + listtim[i].index.ToString();
            ConsoleHelper.Lists(stroklist);

            string strok = Add.AddConsoleEvent();
            string[] partstrok = strok.Split(';');

            TimelineEvent events = new TimelineEvent();
            events.timeevent = DateTimeExtensions.ParseDate((string)partstrok[ReadFiles.eventDate]);
            events.event1 = (string)partstrok[ReadFiles.eventOp];
            events.index = int.Parse((string)partstrok[ReadFiles.eventInd]);

            string json = JsonConvert.SerializeObject(events, Formatting.Indented);

            string jsonTimeline = File.ReadAllText("timeline.json");
            TimelineEvent[] timeline = JsonConvert.DeserializeObject<TimelineEvent[]>(jsonTimeline);

            TimelineEvent[] tork = new TimelineEvent[timeline.Length + 1];
            for (int i = 0; i < tork.Length; ++i)
            {
                if (i < timeline.Length)
                {
                    tork[i].timeevent = timeline[i].timeevent;
                    tork[i].event1 = timeline[i].event1;
                    tork[i].index = timeline[i].index;
                }
                else
                {
                    tork[i].timeevent = events.timeevent;
                    tork[i].event1 = events.event1;
                    tork[i].index = events.index;
                }
            }

            string jsonT = JsonConvert.SerializeObject(tork, Formatting.Indented);
            StreamWriter f = new StreamWriter("timeline.json");
            f.WriteLine(jsonT);
            f.Close();
        }
       

       
       public static void AnswerEventJson()
        {
            Console.Clear();
            string jsonTimeline = File.ReadAllText("timeline.json");
            TimelineEvent[] timeline = JsonConvert.DeserializeObject<TimelineEvent[]>(jsonTimeline);
            (int year, int month, int day) = DeltaMinAndMaxDay.DeltaMinAndMaxDays(timeline);
            Console.WriteLine($"Между макс и мин датами прошло: {year} лет, {month} месяцев и {day} дней");
        }
       
    }
}
