﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class DataSelection
    {
        public static int NameCleek(string[] items, int[] index)
        {
            if (index.Length == 0) return 0;
            ConsoleMenu menu = new ConsoleMenu(items);
            int indexset = 0;
            int menuResult;
            do
            {
                menuResult = menu.PrintMenu2();
                indexset = index[menuResult];
            } while (indexset == 0);
            return indexset;
        }
        public static string NameEditingCleek(string[] name)
        {
            ConsoleMenu menu = new ConsoleMenu(name);
            int menuResult;
            string nameset = null;
            do
            {
                menuResult = menu.PrintMenu();
                nameset = name[menuResult];
            } while (nameset == null);
            return nameset;
        }

    }
}
