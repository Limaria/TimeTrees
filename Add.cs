﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class Add : PeopleRepo
    {

        public static string AddConsolePerson(Person[] people)
        {
            Console.Clear();
            Console.WriteLine("Хотите продолжить?");
            Console.WriteLine("Введите-<Q> если да");
            Console.WriteLine("Введите-<W> если нет");
            bool inputComplete = false;
            string strok = null;
            while (!inputComplete)
            {
                System.ConsoleKeyInfo key = System.Console.ReadKey(true);
                if (key.Key == System.ConsoleKey.Q)
                {
                    Console.Clear();
                    Console.WriteLine("ДОБАВЛЕНИЕ ЧЕЛОВЕКА: ");

                    string deathdate = null;
                    string index = ConsoleHelper.GenerationIndex().ToString();

                    Console.WriteLine("Введите имя: ");
                    string name = Console.ReadLine();

                    Console.WriteLine("Введите дату рождения: ");
                    string birthdate = ConsoleHelper.Limitation();
                    ConsoleHelper.CheckConsoleDate(birthdate);

                    Console.WriteLine("Введите дату смерти: ");
                    deathdate = ConsoleHelper.Limitation();
                    ConsoleHelper.CheckConsoleDate(deathdate);

                    (string indexmom, string indexdad) = Search.Parents(people);

                    return strok = index + ";" + name + ";" + birthdate + ";" + deathdate + ";" + indexmom + ";" + indexdad;
                }
                else if (key.Key == System.ConsoleKey.Q) inputComplete = true;
                else inputComplete = false;
            }
            return strok;
        }

        public static string AddConsoleEvent()
        {
            Console.Clear();
            Console.WriteLine("Хотите продолжить?");
            Console.WriteLine("Введите-<Q> если да");
            Console.WriteLine("Введите-<W> если нет");
            bool inputComplete = false;
            string strok = null;
            while (!inputComplete)
            {
                System.ConsoleKeyInfo key = System.Console.ReadKey(true);
                if (key.Key == System.ConsoleKey.Q)
                {
                    Console.Clear();
                    Console.WriteLine("ДОБАВЛЕНИЕ СОБЫТИЯ: ");

                    Console.WriteLine("Введите дату: ");
                    string timeevent = ConsoleHelper.Limitation();
                    ConsoleHelper.CheckConsoleDate(timeevent);

                    Console.WriteLine("Введите описание события: ");
                    string event1 = Console.ReadLine();

                    Console.WriteLine("Введите индекс человека учавствовавшего в событии: ");
                    string index = Console.ReadLine();
                    ConsoleHelper.CheckConsoleIndex(index);

                   return strok = timeevent + ";" + event1 + ";" + index;

                }
                else if (key.Key == System.ConsoleKey.W)
                {
                    inputComplete = true;
                }
                else inputComplete = false;
            }
            return strok;

        }
    }
}
