﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;


namespace TimeTrees
{
    public class MenuJsonPeople : PeopleRepo
    {
       public static void AddAPersonJson()
        {
            string jsonpeople = File.ReadAllText("people.json");
            Person[] men = JsonConvert.DeserializeObject<Person[]>(jsonpeople);

            string[] stroklist = new string[men.Length];
            for (int i = 0; i < men.Length; i++) stroklist[i] = men[i].index.ToString() + ";" + men[i].name.ToString() + ";" + men[i].birthdaydate.ToString() + ";" + men[i].diedate.ToString() + ";" + men[i].indexmom.ToString() + ";" + men[i].indexdad.ToString();
            ConsoleHelper.Lists(stroklist);

            string strok = Add.AddConsolePerson(men);
            string[] partstrok = strok.Split(';');

            Person person = new Person();
            person.index = int.Parse((string)partstrok[ReadFiles.indexDate]);
            person.name = (string)partstrok[ReadFiles.name];
            person.birthdaydate = DateTimeExtensions.ParseDate((string)partstrok[ReadFiles.birthdayDate]);
            if (partstrok[ReadFiles.dieDate] != null) person.diedate = DateTimeExtensions. ParseDate((string)partstrok[ReadFiles.dieDate]);
            else person.diedate = null;
            if ((string)partstrok[ReadFiles.indexmom] != "") person.indexmom = int.Parse((string)partstrok[ReadFiles.indexmom]);
            else person.indexmom = 0;
            if ((string)partstrok[ReadFiles.indexdad] != "") person.indexdad = int.Parse((string)partstrok[ReadFiles.indexdad]);
            else person.indexdad = 0;


            string json = JsonConvert.SerializeObject(person, Formatting.Indented);



            Person[] tork = new Person[men.Length + 1];
            for (int i = 0; i < tork.Length; ++i)
            {
                if (i < men.Length)
                {
                    tork[i].index = men[i].index;
                    tork[i].name = men[i].name;
                    tork[i].birthdaydate = men[i].birthdaydate;
                    tork[i].diedate = men[i].diedate;
                    tork[i].indexmom = men[i].indexmom;
                    tork[i].indexdad = men[i].indexdad;
                }
                else
                {
                    tork[i].index = person.index;
                    tork[i].name = person.name;
                    tork[i].birthdaydate = person.birthdaydate;
                    tork[i].diedate = person.diedate;
                    tork[i].indexmom = person.indexmom;
                    tork[i].indexdad = person.indexdad;
                }
            }

            string jsonP = JsonConvert.SerializeObject(tork, Formatting.Indented);
            StreamWriter f = new StreamWriter("people.json");
            f.WriteLine(jsonP);
            f.Close();
        }

        public static void PersonEditionJson()
        {
            List<string> nameset = new List<string>();
            string jsonpeople = File.ReadAllText("people.json");
            Person[] people = JsonConvert.DeserializeObject<Person[]>(jsonpeople);

            foreach (var name in people) nameset.Add(name.name);
            string chosenName = DataSelection.NameEditingCleek(nameset.ToArray());

            for (int i = 0; i < people.Length; i++)
            {
                if (chosenName == people[i].name)
                {
                    object[] date = new object[6];
                    date[0] = people[i].index;
                    date[1] = people[i].name;
                    date[2] = people[i].birthdaydate;
                    date[3] = people[i].diedate;
                    date[4] = people[i].indexmom;
                    date[5] = people[i].indexdad;
                    DataEditings.ConsoleRedactDate(date, chosenName);

                    System.ConsoleKeyInfo key = System.Console.ReadKey(true);
                    if (key.Key == System.ConsoleKey.NumPad1)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите имя: ");
                        people[i].name = Console.ReadLine();
                    }

                    else if (key.Key == System.ConsoleKey.NumPad2)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите дату рождения: ");
                        people[i].birthdaydate = DateTimeExtensions.ParseDate(ConsoleHelper.Limitation());
                    }

                    else if (key.Key == System.ConsoleKey.NumPad3)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите дату смерти: ");
                        people[i].diedate = DateTimeExtensions.ParseDate(ConsoleHelper.Limitation());
                    }

                    else if (key.Key == System.ConsoleKey.NumPad4)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите мать: ");
                        people[i].indexmom = Search.SearchPerson(people);
                    }

                    else if (key.Key == System.ConsoleKey.NumPad5)
                    {
                        Console.Clear();
                        Console.WriteLine("Введите отца: ");
                        people[i].indexdad = Search.SearchPerson(people);
                    }
                    else if (key.Key == System.ConsoleKey.Enter)
                    {
                       Exits.Exit();
                    }
                    else PersonEditionJson();
                }
            }


            string jsonP = JsonConvert.SerializeObject(people, Formatting.Indented);
            StreamWriter f = new StreamWriter("people.json");
            f.WriteLine(jsonP);
            f.Close();
        }

        public static void AnswerPersonJson()
        {
            Console.Clear();
            string jsonPerson = File.ReadAllText("people.json");
            Person[] people = JsonConvert.DeserializeObject<Person[]>(jsonPerson);
            PeopleBirth.PeopleBirthday(people);
        }
    }
}
