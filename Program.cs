﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    class Program 
    {
        delegate void method();
        public static void Main()
        {
            string[] items = { "Файл .csv", "Файл .json", "Выход" };
            method[] methods = new method[] { AllMenu.Menu, AllMenu.MenuJson, Exits.Exit };
            ConsoleMenu menu = new ConsoleMenu(items);
            int menuResult;
            do
            {
                menuResult = menu.PrintMenu();
                methods[menuResult]();
                Console.WriteLine("Для продолжения нажмите любую клавишу");
                Console.ReadKey();
            } while (menuResult != items.Length - 1);
        }
    }
}


