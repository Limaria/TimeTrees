﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class ReadFiles
    { 
        public const int eventDate = 0;
        public const int eventOp = 1;
        public const int eventInd = 2;

        public const int indexDate = 0;
        public const int name = 1;
        public const int birthdayDate = 2;
        public const int dieDate = 3;
        public const int indexmom = 4;
        public const int indexdad = 5;

        public static object[][] ReadData(string[] timeline)
        {
            object[][] number = new object[timeline.Length][];

            for (var i = 0; i < timeline.Length; i++)
            {
                var part = timeline[i];
                string[] split = part.Split(';');
                number[i] = split;
            }
            return number;
        }

        public static TimelineEventsRepo.TimelineEvent[] ReadTimelineEvent(string[] timeline)
        {
            var read = ReadData(timeline);
            TimelineEventsRepo.TimelineEvent[] holiday = new TimelineEventsRepo.TimelineEvent[read.Length];

            for (var i = 0; i < read.Length; i++)
            {
                var parts = read[i];
                TimelineEventsRepo.TimelineEvent events = new TimelineEventsRepo.TimelineEvent();
                events.timeevent = DateTimeExtensions.ParseDate((string)parts[eventDate]);
                events.event1 = (string)parts[eventOp];
                events.index = int.Parse((string)parts[eventInd]);

                holiday[i] = events;
            }
            return holiday;
        }

        public static PeopleRepo.Person[] ReadPerson(string[] people)
        {
            var read = ReadData(people);
            PeopleRepo.Person[] men = new PeopleRepo.Person[read.Length];

            for (var i = 0; i < read.Length; i++)
            {
                var parts = read[i];

                PeopleRepo.Person person = new PeopleRepo.Person();
                person.index = int.Parse((string)parts[indexDate]);
                person.name = (string)parts[name];
                person.birthdaydate = DateTimeExtensions.ParseDate((string)parts[birthdayDate]);
                if (parts[dieDate] != null) person.diedate = DateTimeExtensions.ParseDate((string)parts[dieDate]);
                else person.diedate = null;
                if ((string)parts[indexmom] != "") person.indexmom = int.Parse((string)parts[indexmom]);
                else person.indexmom = 0;
                if ((string)parts[indexdad] != "") person.indexdad = int.Parse((string)parts[indexdad]);
                else person.indexdad = 0;
                men[i] = person;
            }
            return men;
        }



    }
}
