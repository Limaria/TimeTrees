﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class Search : PeopleRepo
    {
        public static int SearchPerson(Person[] people)
        {
            Console.Clear();
            List<int> indexset = new List<int>();
            List<string> nameset = new List<string>();
            int index = 0;
            Console.WriteLine("Начните вводить имя: ");

            bool inputComplete = false;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            while (!inputComplete)
            {
                System.ConsoleKeyInfo key = System.Console.ReadKey(true);
                if (key.Key == System.ConsoleKey.Enter)
                {
                    inputComplete = true;
                    index = DataSelection.NameCleek(nameset.ToArray(), indexset.ToArray());
                }
                else
                {
                    sb.Append(key.KeyChar);
                    System.Console.Write(key.KeyChar.ToString());
                    nameset = new List<string>();
                    foreach (var name in people)
                    {
                        string nameconsole = name.name;
                        char[] letter = nameconsole.ToCharArray();
                        for (int i = sb.Length - 1; i > -1; i--)
                        {
                            if (sb[i] == letter[i])
                            {
                                if (nameset.Contains(name.name)) break;
                                else
                                {
                                    nameset.Add(name.name);
                                    indexset.Add(name.index);
                                }
                            }
                            else break;
                        }
                    }
                    Console.SetCursorPosition(0, 2);
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine(new string(' ', 30));
                    }

                    Console.SetCursorPosition(0, 2);
                    foreach (var item in nameset) Console.WriteLine(item);
                    Console.SetCursorPosition(sb.Length, 1);
                }
            }
            return index;
        }


        public static (string, string) Parents(Person[] people)
        {
            Console.Clear();
            string indexmom = null;
            string indexdad = null;
            Console.WriteLine("Хотите ввести родителей?");

            Console.WriteLine("Введите-<Q> если да");
            Console.WriteLine("Введите-<W> если нет");

            System.ConsoleKeyInfo key = System.Console.ReadKey(true);
            if (key.Key == System.ConsoleKey.Q)
            {
                Console.Clear();
                Console.WriteLine("Поиск матери: ");
                Console.ReadKey();
                int mom = SearchPerson(people);
                if (mom != 0) indexmom = mom.ToString();

                Console.Clear();
                Console.WriteLine("Поиск отца: ");
                Console.ReadKey();
                int dad = SearchPerson(people);
                if (dad != 0) indexdad = dad.ToString();

                return (indexmom, indexdad);
            }
            else if (key.Key == System.ConsoleKey.W)
            {
                return (indexmom, indexdad);
            }
            else return Parents(people);
        }
    }
}
