﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class DataEditings : ReadFiles
    {
        public static object[] Redakt(object[] parts, PeopleRepo.Person[] people)
        {
            System.ConsoleKeyInfo key = System.Console.ReadKey(true);
            if (key.Key == System.ConsoleKey.NumPad1)
            {
                Console.Clear();
                Console.WriteLine("Введите имя: ");
                parts[name] = Console.ReadLine();
            }

            else if (key.Key == System.ConsoleKey.NumPad2)
            {
                Console.Clear();
                Console.WriteLine("Введите дату рождения: ");
                parts[birthdayDate] = ConsoleHelper.Limitation();
            }

            else if (key.Key == System.ConsoleKey.NumPad3)
            {
                Console.Clear();
                Console.WriteLine("Введите дату смерти: ");
                parts[dieDate] = ConsoleHelper.Limitation();
            }

            else if (key.Key == System.ConsoleKey.NumPad4)
            {
                Console.Clear();
                Console.WriteLine("Введите мать: ");
                parts[indexmom] = Search.SearchPerson(people).ToString();
            }

            else if (key.Key == System.ConsoleKey.NumPad5)
            {
                Console.Clear();
                Console.WriteLine("Введите отца: ");
                parts[indexdad] =Search.SearchPerson(people).ToString();
            }
            else if (key.Key == System.ConsoleKey.Enter)
            {
                Exits.Exit();
            }
            else
            {
                Redakt(parts, people);
            }
            return parts;
        }


        public static string[][] RedaktFile(string[] people_file)
        {
            List<string> nameset = new List<string>();
            PeopleRepo.Person[] people = ReadPerson(people_file);
            foreach (var name in people) nameset.Add(name.name);
            string chosenName = DataSelection.NameEditingCleek(nameset.ToArray());

            var read = ReadData(people_file);
            string[][] men = new string[read.Length][];

            for (int i = 0; i < read.Length; i++)
            {
                var parts = read[i];
                if (chosenName != parts[name].ToString())
                {
                    string[] part = new string[parts.Length];
                    for (int j = 0; j < read[i].Length; ++j) part[j] = (string)parts[j];
                    men[i] = part;
                }

                else
                {
                    ConsoleRedactDate(parts, chosenName);
                    parts = Redakt(parts, people);
                    string[] part = new string[parts.Length];
                    for (int j = 0; j < read[i].Length; ++j) part[j] = (string)parts[j];
                    men[i] = part;
                }

            }
            return men;
        }

        public static void ConsoleRedactDate(object[] people, string chosenName)
        {
            for (int i = 0; i < people.Length; i++)
            {
                if (chosenName == (string)people[name])
                {
                    Console.Clear();
                    Console.WriteLine("Что хотите изменить?");
                    Console.WriteLine("Индекс: " + people[indexDate]);
                    Console.WriteLine($"1. Имя: " + people[name]);
                    Console.WriteLine($"2. Дата рождения: " + people[birthdayDate]);
                    if (people[dieDate] == null) Console.WriteLine($"3. Дата смерти: неизвестна. ");
                    else Console.WriteLine($"3. Дата смерти: " + people[dieDate]);

                    if (people[indexmom] != null) Console.WriteLine($"4. Мать: " + people[indexmom]);
                    else Console.WriteLine($"4. Мать: неизвестна. ");

                    if (people[indexdad] != null) Console.WriteLine($"5. Отец: " + people[indexdad]);
                    else Console.WriteLine($"5. Отец: неизвестен. ");
                }
            }
        }
    }
}
