﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    class DateTimeExtensions
    {
        public static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        date = default;
                    }
                }
            }
            return date;
        }
    }
}
