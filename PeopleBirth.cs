﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    public class PeopleBirth : PeopleRepo
    {
        public static void PeopleBirthday(Person[] people)
        {
            int i = 0;
            foreach (var date in people)
            {
                DateTime birthdate = date.birthdaydate;

                DateTime life = DateTime.Now;
                if (date.diedate != null)
                {
                    life = (DateTime)date.diedate;
                }

                if (DateTime.IsLeapYear(birthdate.Year) && (life.Year - birthdate.Year <= 20))
                {
                    Console.WriteLine(date.name);
                }
                i = i + 1;

            }
        }
    }
}
