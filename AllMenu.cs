﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class AllMenu
    {
        delegate void method();
        public static void Menu()
        {
            Console.Clear();
            string[] items = { "Добавить событие", "Добавить человека", "Редактирование данных", "Разница между макс и мин датами событий", "Имя человека родившегося в високосный год", "Выход" };
            method[] methods = new method[] {MenuCsvTimeLine.AddAnEvent, MenuCsvPeople.AddAPerson, MenuCsvPeople.DataEditing, MenuCsvTimeLine.AnswerEvent, MenuCsvPeople.AnswerPerson, Exits.Exit };
            ConsoleMenu menu = new ConsoleMenu(items);
            int menuResult;
            do
            {
                menuResult = menu.PrintMenu();
                methods[menuResult]();
                Console.WriteLine("Для продолжения нажмите любую клавишу");
                Console.ReadKey();
            } while (menuResult != items.Length - 1);
        }

        public static void MenuJson()
        {
            Console.Clear();
            string[] items = { "Добавить событие", "Добавить человека", "Редактирование", "Разница между макс и мин датами событий", "Имя человека родившегося в високосный год", "Выход" };
            method[] methods = new method[] { MenuJsonTimeLine.AddAnEventJson, MenuJsonPeople.AddAPersonJson, MenuJsonPeople.PersonEditionJson, MenuJsonTimeLine.AnswerEventJson, MenuJsonPeople.AnswerPersonJson, Exits.Exit };
            ConsoleMenu menu = new ConsoleMenu(items);
            int menuResult;
            do
            {
                menuResult = menu.PrintMenu();
                methods[menuResult]();
                Console.WriteLine("Для продолжения нажмите любую клавишу");
                Console.ReadKey();
            } while (menuResult != items.Length - 1);
        }
    }
}
