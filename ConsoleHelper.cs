﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class ConsoleHelper : PeopleRepo
    {
        public static void CheckConsoleIndex(string consolIndex)
        {
            string[] people_file = File.ReadAllLines("people.csv");
            Person[] people = ReadFiles.ReadPerson(people_file);
            int indexes = 0;

            if (consolIndex == null) return;

            foreach (var date in people)
            {
                indexes = date.index;
                if (indexes == int.Parse(consolIndex)) break;
            }

            if (indexes != int.Parse(consolIndex))
            {
                Console.WriteLine("Данного человека нет в списке. Попробуйте снова.");
                Console.ReadKey();
                Console.Clear();
                Program.Main();
            }
        }

        public static void CheckConsoleDate(string number)
        {
            string[] split = number.Split('-');
            if (split.Length < 2)
            {
                var part0 = split[0];
                string g = null;
                for (int i = 0; i < part0.Length; i++) g = g + part0[i];
                if (g != null)
                {
                    int k = int.Parse(g);
                    if (k > 9999)
                    {
                        Console.WriteLine("Неверная дата года");
                        Console.ReadKey();
                        Console.Clear();
                        Program.Main();
                    }
                }
                else
                {
                    Console.WriteLine("Дата пропущена");
                    Program.Main();
                }
            }

            else if (split.Length < 3)
            {
                var part0 = split[0];
                var part1 = split[1];
                string g = null;
                string n = null;

                for (int i = 0; i < part0.Length; i++) g = g + part0[i];
                int k = int.Parse(g);
                if (k > 9999)
                {
                    Console.WriteLine("Неверная дата года");
                    Console.ReadKey();
                    Console.Clear();
                    Program.Main();
                }


                for (int i = 0; i < part1.Length; i++) n = n + part1[i];
                int s = int.Parse(n);

                if (s > 12)
                {
                    Console.WriteLine("Неверная дата месяца");
                    Console.ReadKey();
                    Console.Clear();
                    Program.Main();
                }
            }

            else if (split.Length < 4)
            {
                var part0 = split[0];
                var part1 = split[1];
                string g = null;
                string n = null;
                string d = null;

                for (int i = 0; i < part0.Length; i++) g = g + part0[i];
                int k = int.Parse(g);
                if (k > 9999)
                {
                    Console.WriteLine("Неверная дата года");
                    Console.ReadKey();
                    Console.Clear();
                    Program.Main();
                }


                for (int i = 0; i < part1.Length; i++) n = n + part1[i];
                int s = int.Parse(n);
                if (s > 12)
                {
                    Console.WriteLine("Неверная дата месяца");
                    Console.ReadKey();
                    Console.Clear();
                    Program.Main();
                }

                for (int i = 0; i < part1.Length; i++) d = d + part1[i];
                int v = int.Parse(d);
                if (v > 31)
                {
                    Console.WriteLine("Неверная дата дня");
                    Console.ReadKey();
                    Console.Clear();
                    Program.Main();
                }
            }
            else
            {
                Console.WriteLine("Неверная дата");
                Console.ReadKey();
                Console.Clear();
                Program.Main();
            }
        }

        public static string Limitation()
        {
            bool inputComplete = false;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            while (!inputComplete)
            {
                System.ConsoleKeyInfo key = System.Console.ReadKey(true);
                if (key.Key == System.ConsoleKey.Enter)
                {
                    inputComplete = true;
                }
                else if (key.Key == System.ConsoleKey.Divide || key.Key == System.ConsoleKey.OemPeriod || key.Key == System.ConsoleKey.Decimal || key.Key == System.ConsoleKey.OemComma || key.Key == System.ConsoleKey.OemMinus)
                {
                    sb.Append("-");
                    Console.Write("-");
                }
                else if (char.IsDigit(key.KeyChar))
                {
                    sb.Append(key.KeyChar);
                    System.Console.Write(key.KeyChar.ToString());
                }
            }
            Console.WriteLine();
            return sb.ToString();
        }

        public static int GenerationIndex()
        {
            string[] people_file = File.ReadAllLines("people.csv");
            Person[] people = ReadFiles.ReadPerson(people_file);
            int indexes = 0;
            foreach (var date in people)
            {
                indexes = date.index;
            }
            return ++indexes;
        }

        public static void Lists(string[] timeline_file)
        {
            Console.Clear();
            Console.WriteLine("Имеющиеся данные:");
            for (int i = 0; i < timeline_file.Length; i++)
            {
                Console.WriteLine(timeline_file[i]);
            }
            Console.ReadKey();
        }
     }
 }

