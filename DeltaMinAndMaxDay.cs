﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    public class DeltaMinAndMaxDay : TimelineEventsRepo
    {
        public static(int, int, int) DeltaMinAndMaxDays(TimelineEvent[] timeLine)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDays(timeLine);
            return (maxDate.Year - minDate.Year, maxDate.Month - minDate.Month, maxDate.Day - minDate.Day);
        }

        static (DateTime, DateTime) GetMinAndMaxDays(TimelineEvent[] timeline)
        {
            DateTime minData = DateTime.MaxValue, maxData = DateTime.MinValue;
            foreach (var date in timeline)
            {
                DateTime date1 = date.timeevent;
                if (date1 < minData) minData = date1;
                if (date1 > maxData) maxData = date1;
            }
            return (minData, maxData);
        }
    }
}
