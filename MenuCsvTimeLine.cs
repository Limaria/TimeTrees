﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    public class MenuCsvTimeLine : TimelineEventsRepo
    {
        
        public static void AddAnEvent()
        {
            string[] timeline_file = File.ReadAllLines("timeline.csv");
            ConsoleHelper.Lists(timeline_file);
            string strok = Add.AddConsoleEvent();
            StreamWriter f = new StreamWriter("timeline.csv", true);
            f.WriteLine(strok);
            f.Close();
        }

         public static void AnswerEvent()
        {
            Console.Clear();
            string[] timeline_file = File.ReadAllLines("timeline.csv");
            TimelineEvent[] timeline = ReadFiles.ReadTimelineEvent(timeline_file);
            (int year, int month, int day) = DeltaMinAndMaxDay.DeltaMinAndMaxDays(timeline);
            Console.WriteLine($"Между макс и мин датами прошло: {year} лет, {month} месяцев и {day} дней");
        }
       

    }
}
