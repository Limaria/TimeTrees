﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;
namespace TimeTrees
{
    class MenuCsvPeople : PeopleRepo
    {
       public static void AddAPerson()
        {
            string[] people_file = File.ReadAllLines("people.csv");
            Person[] people = ReadFiles.ReadPerson(people_file);
            ConsoleHelper.Lists(people_file);
            string strok = Add.AddConsolePerson(people);
            StreamWriter f = new StreamWriter("people.csv", true);
            f.WriteLine(strok);
            f.Close();
        }

       public static void DataEditing()
        {
            string[] people_file = File.ReadAllLines("people.csv");
            string[][] people = DataEditings.RedaktFile(people_file);
            StreamWriter f = new StreamWriter("people.csv");
            foreach (var men in people)
            {
                f.WriteLine(men[ReadFiles.indexDate] + ";" + men[ReadFiles.name] + ";" + men[ReadFiles.birthdayDate] + ";" + men[ReadFiles.dieDate] + ";" + men[ReadFiles.indexmom] + ";" + men[ReadFiles.indexdad]);
            }

            f.Close();
        }

        public static void AnswerPerson()
        {
            Console.Clear();
            string[] people_file = File.ReadAllLines("people.csv");
            Person[] people = ReadFiles.ReadPerson(people_file);
            PeopleBirth.PeopleBirthday(people);
        }

    }
}
